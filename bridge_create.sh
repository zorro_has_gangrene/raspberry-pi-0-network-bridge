#!/bin/bash
if [ $# -eq 0 ]; then
  echo "Usage ./bridge_create.sh [argument]"
  echo "Where argument is the physical interface you want to add to the bridge"
  exit 1
fi
ip link add name pi_bridge type bridge
ip link set $1 up
ip link set $1 master pi_bridge
dhcpcd pi_bridge
