#!/bin/bash
hexchars="0123456789ABCDEF"
end=$( for i in {1..6} ; do echo -n ${hexchars:$(( $RANDOM % 16 )):1} ; done | sed -e 's/\(..\)/:\1/g' )
MAC1="00:60:2F$end"
end=$( for i in {1..6} ; do echo -n ${hexchars:$(( $RANDOM % 16 )):1} ; done | sed -e 's/\(..\)/:\1/g' )
MAC2="00:60:2F$end"
echo "options g_ether host_addr=$MAC1 dev_addr=$MAC2" > g_ether.conf
echo "Use the following MAC in 10-network.rules $MAC1"
