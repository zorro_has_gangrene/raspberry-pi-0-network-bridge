#!/bin/bash
echo "entering enpi.sh" >> /home/ed/monitor.log
if [ $# -eq 0 ]; then
  echo "Usage ./enpi.sh [argument]"
  echo "Where argument is the pi number you want to add to the bridge"
  exit 1
fi
ip link set enpi$1 up
ip link set enpi$1 master pi_bridge
exit 0
